package com.eurovision.service;

import com.eurovision.model.City;
import com.eurovision.repository.CityPagingAndSortingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class SequenceServiceTest{

    @Mock
    CityPagingAndSortingRepository cityRepository;

    @InjectMocks
    SequenceService sequenceService;

    Iterable<City> expectedCities;

    @Before
    public void setup(){
        expectedCities = getCities();
        when(cityRepository.findAll(any(Sort.class))).thenReturn(expectedCities);
    }


    @Test
    public void testService(){
        List<Integer> biggestSequence = sequenceService.getBiggestSequence();

        assertEquals(4,biggestSequence.size());
        assertEquals(3, biggestSequence.get(0).intValue());
        assertEquals(6, biggestSequence.get(1).intValue());
        assertEquals(9, biggestSequence.get(2).intValue());
        assertEquals(18, biggestSequence.get(3).intValue());
    }

    /**
     * Build a set of cities
     * @return
     */
    public Iterable<City> getCities(){

        List<City> cities = new ArrayList<>();

        City city1 = new City();
        city1.setId(10);

        City city2 = new City();
        city2.setId(3);

        City city3 = new City();
        city3.setId(6);

        City city4 = new City();
        city4.setId(9);

        City city5 = new City();
        city5.setId(18);

        cities.add(city1);
        cities.add(city2);
        cities.add(city3);
        cities.add(city4);
        cities.add(city5);

        return cities;
    }



}