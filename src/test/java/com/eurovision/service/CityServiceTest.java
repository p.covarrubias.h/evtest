package com.eurovision.service;

import com.eurovision.model.City;
import com.eurovision.repository.CityPagingAndSortingRepository;
import com.eurovision.utils.CityHelperTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CityServiceTest{

    @Mock CityPagingAndSortingRepository cityRepository;

    @InjectMocks
    CityService cityService;

    @Mock Pageable pageable;

    Page<City> expectedCities;

    @Before
    public void setup(){
        expectedCities = CityHelperTest.getExpectedCities();
        when(cityRepository.findAll(pageable)).thenReturn(expectedCities);
    }

    @Test
    public void testService(){
        Page<City> citiesService = cityService.getCities(pageable);

        assertEquals(expectedCities,citiesService);
        assertEquals(4, citiesService.stream().count());
        assertEquals(CityHelperTest.CITY1_NAME, citiesService.getContent().get(0).getName());
        assertEquals(CityHelperTest.CITY2_NAME, citiesService.getContent().get(1).getName());
        assertEquals(1,citiesService.getContent().get(0).getId().intValue());
        assertEquals(2, citiesService.getContent().get(1).getId().intValue());
    }
}