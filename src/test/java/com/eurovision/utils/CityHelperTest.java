package com.eurovision.utils;

import com.eurovision.model.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

public class CityHelperTest{
    public static final String CITY1_NAME = "MADRID";
    public static final String CITY2_NAME = "GENEVA";
    public static final String CITY3_NAME = "BARCELONA";
    public static final String CITY4_NAME = "AMSTERDAM";

    private CityHelperTest(){}

    public static Page<City> getExpectedCities(){

        City city1 = new City();
        city1.setName(CITY1_NAME);
        city1.setId(1);

        City city2 = new City();
        city2.setName(CITY2_NAME);
        city2.setId(2);

        City city3 = new City();
        city3.setName(CITY3_NAME);
        city3.setId(3);

        City city4 = new City();
        city4.setName(CITY4_NAME);
        city4.setId(4);

        List<City> cities = new ArrayList<>();
        cities.add(city1);
        cities.add(city2);
        cities.add(city3);
        cities.add(city4);

        return new PageImpl<>(cities);
    }
}
