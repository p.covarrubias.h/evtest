package com.eurovision.rest;

import com.eurovision.model.City;
import com.eurovision.service.CityService;
import com.eurovision.utils.CityHelperTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CityControllerTest{

    @Mock
    CityService cityService;

    @InjectMocks
    CityController cityController;

    @Mock Pageable pageable;

    Page<City> expectedCities;

    @Before
    public void setUp(){
        expectedCities = CityHelperTest.getExpectedCities();
        when(cityService.getCities(pageable)).thenReturn(expectedCities);
    }

    @Test
    public void testController(){
        Page<City> citiesController = cityController.cities(pageable);

        assertEquals(expectedCities,citiesController);
        assertEquals(4, citiesController.stream().count());
        assertEquals(CityHelperTest.CITY1_NAME, citiesController.getContent().get(0).getName());
        assertEquals(CityHelperTest.CITY2_NAME, citiesController.getContent().get(1).getName());

    }

}