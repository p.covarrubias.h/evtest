package com.eurovision.service;

import com.eurovision.model.City;
import com.eurovision.repository.CityPagingAndSortingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SequenceService implements IBiggestSequence{

    @Autowired CityPagingAndSortingRepository cityRepository;

    @Override
    public List<Integer> getBiggestSequence(){

        //Retrieve the list of cities from database ordered by name.
        Iterable<City> cities = cityRepository.findAll(Sort.by("name"));

        //Transform iterable to list
        List<City> cityList = new ArrayList<>();
        cities.forEach(cityList::add);

        //Obtain a list of IDs
        List<Integer> idsList = cityList.stream().map(City::getId).collect(Collectors.toList());

        //Iterate over each id and obtains its sequence incremental.
        List<List<Integer>> combinations = new ArrayList<>();
        for(int index = 0; index<idsList.size(); index++){
            List<Integer> combination = generateCombination(index, idsList);
            combinations.add(combination);
        }

        //Retrieve the largest list if present, else return an empty collection.
        return combinations.stream().max(Comparator.comparingInt(List::size)).orElse(Collections.emptyList());
    }

    /**
     *  Gets the incremental sequence for the value placed in the index received.
     *
     * @param index
     * @param idsList
     * @return
     */
    public List<Integer> generateCombination(int index, List<Integer> idsList){
        List<Integer> integerTempList = new ArrayList<>();
        integerTempList.add(idsList.get(index));

        for(int indexB = index+1; indexB < idsList.size(); indexB++){

            //If the ID of indexB is higher than last ID stored in aux list, we added it.
            if(idsList.get(indexB) > integerTempList.get(integerTempList.size()-1)){
                integerTempList.add(idsList.get(indexB));
            }
        }
        return integerTempList;
    }

}
