package com.eurovision.service;

import com.eurovision.model.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ICityService{

    /**
     * Returns a Page of cities
     *
     * @param pageable
     * @return
     */
    public Page<City> getCities(Pageable pageable);
}
