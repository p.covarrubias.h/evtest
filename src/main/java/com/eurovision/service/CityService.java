package com.eurovision.service;

import com.eurovision.model.City;
import com.eurovision.repository.CityPagingAndSortingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CityService implements ICityService{

    @Autowired CityPagingAndSortingRepository cityRepository;

    public Page<City> getCities(Pageable pageable){
        return cityRepository.findAll(pageable);
    }

}
