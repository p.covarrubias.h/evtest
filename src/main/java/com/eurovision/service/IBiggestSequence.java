package com.eurovision.service;
import java.util.List;

public interface IBiggestSequence{

    /**
     * It returns the biggest sequence of ids of the cities in order ascendant
     *
     * @return
     */
    public List<Integer> getBiggestSequence();
}
