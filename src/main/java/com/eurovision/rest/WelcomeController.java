package com.eurovision.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class WelcomeController{

    /**
     * It returns a simple welcome message.
     *
     * @return
     */
    @RequestMapping("/")
    public String welcome() {
        return "Welcome to Eurovision Services";
    }
}

