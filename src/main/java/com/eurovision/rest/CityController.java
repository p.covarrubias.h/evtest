package com.eurovision.rest;

import com.eurovision.model.City;
import com.eurovision.service.IBiggestSequence;
import com.eurovision.service.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cities")
public class CityController{

    @Autowired
    ICityService cityService;

    @Autowired
    IBiggestSequence bigSequenceService;

    /**
     * It returns a the page of cities requested with the size specified.
     *
     * @param pageable
     * @return A page of cities.
     */
    @GetMapping(path = "/queryByPage", produces = "application/json")
    public @ResponseBody Page<City> cities(Pageable pageable){
        return cityService.getCities(pageable);
    }

    /**
     * It returns the biggest sequence based on the City.Id ascendant.
     *
     * @return The list of ids of the biggest sequence
     */
    @GetMapping(path = "/biggestSequence", produces = "application/json")
    public @ResponseBody List<Integer> biggestSequence(){
        return bigSequenceService.getBiggestSequence();
    }

}
