package com.eurovision.repository;

import com.eurovision.model.City;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityPagingAndSortingRepository extends PagingAndSortingRepository<City, Integer>{
}
